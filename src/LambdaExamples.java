import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by qsoft on 2/18/16.
 */
public class LambdaExamples {

    public static void main(String[] args) {
        testComplicatedOperation();
//        testBinaryOperation();
//        testUnaryOperation();
        testArraySort();
    }

    private static void testArraySort() {
        String[] testStrings = new String[]{"aaaa", "b"};
        Arrays.sort(testStrings, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return (s1.length() - s2.length());
            }
        });

        Arrays.sort(testStrings,
                (String s1, String s2) -> {
                    return (s1.length() - s2.length());
                });
    }

    interface ComplicatedOperation {
        String process(int n);
    }

    public static void testComplicatedOperation() {
        ComplicatedOperation commaSeparatedSequence = (int n) -> {
            String s = "";
            for (int i = 0; i < n; i++) s += i + ",";
            return s;
        };

        ComplicatedOperation operation = new ComplicatedOperation() {
            @Override
            public String process(int n) {
                return "(" + n + ")";
            }
        };

        ComplicatedOperation quote = (int n) -> {
            return "(" + n + ")";
        };

        System.out.println("1..10 = " + commaSeparatedSequence.process(10));
        System.out.println("(20) = " + quote.process(20));
    }

    interface BinaryOperation {
        int operation(int a, int b);
    }

    public static void testBinaryOperation() {
        BinaryOperation add = (int a, int b) -> {
            return a + b;
        };   // full

        BinaryOperation subtract = (int a, int b) -> a - b;      // without return and {}

        BinaryOperation multiply = (a, b) -> a * b;       // without parameter type and {}

        System.out.println("2 + 3 = " + add.operation(2, 3));
        System.out.println("2 - 3 = " + subtract.operation(2, 3));
        System.out.println("2 * 3 = " + multiply.operation(2, 3));
    }

    interface UnaryOperation {
        int operation(int a);
    }

    public static void testUnaryOperation() {
        UnaryOperation square = (a) -> a * a;   // without parameter type and {}

        UnaryOperation negate = a -> -a;      // without () and parameter type and {}

        System.out.println("2^2 = " + square.operation(2));
        System.out.println("2 negate = " + negate.operation(2));
    }
}
