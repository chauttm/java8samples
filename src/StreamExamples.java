import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qsoft on 1/25/16.
 */
public class StreamExamples {
    static class Sum {
        private int sum = 0;
        public void add(int value) {sum += value;}
        public int getSum() { return sum; }
    }

    public static boolean greaterThan3(int value) {
           return value > 3;
    }

    public static void main(String[] args) throws ParseException {

        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

        System.out.println("-----------foreach");
        numbers.forEach(System.out::println);

        Sum s = new Sum();
        numbers.forEach(s::add);
        System.out.println("Sum = " + s.getSum());

        System.out.println("-----------stream and foreach");
        numbers.stream().forEach(System.out::println);

        System.out.println("-----------stream and limit");
        numbers.stream().limit(3).forEach(System.out::println);

        System.out.println("-----------stream and sorted");
        numbers.stream().sorted().forEach(System.out::println);

        System.out.println("-----------stream and filter");
        numbers.stream().filter(StreamExamples::greaterThan3).forEach(System.out::println);

        System.out.println("-----------");

//get list of unique squares
//        List<Integer> squaresList = numbers.stream().map( i -> i*i).collect(Collectors.toList());
//
//        squaresList.forEach(item -> {System.err.print(item + ",");});
    }
}
